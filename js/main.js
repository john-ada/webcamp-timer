(function(angular) {
    'use strict';

    angular.module('WebcampTimerApp',['timer'])
    .controller('WebcampTimerController',['$scope', function ($scope) {
        var mainTimer = document.getElementById('main-timer'),
            secondValue = 200;

        $scope.start = function($event) {
            $event.preventDefault();

            mainTimer.start();
        }

        $scope.stop = function($event) {
            $event.preventDefault();

            mainTimer.clear();
        }

        $scope.$watch('seconds', function() {
            $scope.count = calculateCount();
        });

        $scope.$watch('minutes', function() {
            $scope.count = calculateCount();
        });

        $scope.$watch('hours', function() {
            $scope.count = calculateCount();
        });

        /* Just in case. :3 */
        // $scope.$on('timer-tick', function (event, args) {
            
        // });

        function calculateCount() {
            var hour   = $scope.hours,
                minute = $scope.minutes,
                second = $scope.seconds;

            if (typeof hour === 'undefined' || isNaN(hour)) {
                hour = 0;
            } else {
                hour = parseInt(hour) * 60 * 60 * secondValue;
            }

            if (typeof minute === 'undefined' || isNaN(minute)) {
                minute = 0;
            } else {
                minute = parseInt(minute) * 60 * secondValue;
            }

            if (typeof second === 'undefined' || isNaN(second)) {
                second = 0;
            } else {
                second = parseInt(second) * secondValue;
            }

            return hour + minute + second;
        }

    }])
    .filter('millis', function() {
        return function(input) {
            var num = parseFloat(input / 100 / 2) - parseInt(input / 100 / 2) + "";

            return isNaN(num) || num.length == 1 ? "00" : (parseInt(num.substring(2, 4)) % 60 + "").length == 1 ? ("0" + parseInt(num.substring(2, 4)) % 60) : (parseInt(num.substring(2, 4)) % 60 + "");
        };
    })
    .filter('seconds', function() {
        return function(input) {
            var num = parseInt(input / 100 / 2) % 60 + "";

            return isNaN(num) ? "00" : num.length < 2 ? "0" + num : num;
        };
    })
    .filter('minutes', function() {
        return function(input) {
            var num = parseInt(input / 100 / 2 / 60) % 60 + "";

            return isNaN(num) ? "00" : num.length < 2 ? "0" + num : num;
        };
    })
    .filter('hours', function() {
        return function(input) {
            var num = parseInt(input / 100 / 2 / 60 / 60) % 60 + "";

            return isNaN(num) ? "00" : num.length < 2 ? "0" + num : num;
        };
    });

})(window.angular);

